mod elnet;


use chrono::{Utc, Duration};

fn main() {
    let mut api = elnet::Api::new();

    if api.token_get().expect("missing elnet token") {
        // println!("new access token {}", api.access_token_get());

        let map = api.meteringpoints().expect("missing metering points");

        //println!("{:#?}", map);

        for meter in map {
            println!(" * Meter data");
            println!("Name: {}", meter.first_consumer_party_name);
            println!("Street: {} {}", meter.street_name, meter.building_number);
            println!("Zip: {} {}", meter.postcode, meter.city_name );
            println!("Supplier name:  {}", meter.balance_supplier_name );
            let mut points = Vec::new();

            for point in meter.child_metering_points {
                println!("Point: {}", point.metering_point_id);

                points.push(point.metering_point_id);
            }

            if points.is_empty() {
                println!("no messure data found");
                continue;
            }

            let now = Utc::now().date();
            let from = now - Duration::weeks(1);

            let messure = api.meterdata(&points, &from, &now ).expect("missing meter data for");

            for data in messure {
                let res = data.data;

                // println!("{:#?}", res);
                for ts in res.time_series {
                    println!( "unit name {}", ts.unit_name );

                    for period in ts.period {
                        println!("{}", period.time_interval.start );

                        for p in period.point {
                            print!( "{} ", p.quantity ) 
                        }
                        println!("{}", period.time_interval.end);
                    }
                }
                println!("{}", data.error_code);
            }
        }
    }
}
