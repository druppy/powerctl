extern crate serde;

use serde::de::{DeserializeOwned};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs;
use std::path::Path;
use chrono::{Date, DateTime, Utc};

use anyhow::{Error, Result};

use reqwest::header::{ACCEPT, CONTENT_TYPE};
use reqwest::Url;

const BASE_API_URL: &str = "https://api.eloverblik.dk/CustomerApi";
const TOKEN_FNAME: &str = "config/token.txt";

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ChildMeteringPoint {
    pub parent_metering_point_id: String,
    pub metering_point_id: String,
    #[serde(rename = "typeOfMP")]
    pub type_of_mp: String,
    pub metering_point_alias: String,
    pub meter_reading_occurrence: String,
    pub meter_number: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Meter {
    pub street_code: String,
    pub street_name: String,
    pub building_number: String,
    pub floor_id: String,
    pub room_id: String,
    pub city_sub_division_name: String,
    pub municipality_code: String,
    pub location_description: String,
    pub settlement_method: String,
    pub meter_reading_occurrence: String,
    pub first_consumer_party_name: String,
    pub second_consumer_party_name: String,
    pub meter_number: String,
    pub consumer_start_date: String,
    pub metering_point_id: String,
    #[serde(rename = "typeOfMP")]
    pub type_of_mp: String,
    pub balance_supplier_name: String,
    pub postcode: String,
    pub city_name: String,
    pub has_relation: bool,
    #[serde(rename = "consumerCVR")]
    pub consumer_cvr: String,
    #[serde(rename = "dataAccessCVR")]
    pub data_access_cvr: String,
    pub child_metering_points: Vec<ChildMeteringPoint>,
}

// Argument for requesting meter time series
#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
struct Meteringpoints {
    metering_point: Vec<String>
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
struct MeteringpointsArgs {
    meteringpoints: Meteringpoints
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TimeInterval {
    pub start: DateTime<Utc>,
    pub end: DateTime<Utc> 
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Point {
    pub position: String,
    #[serde(rename = "out_Quantity.quantity")]
    pub quantity: String,
    #[serde(rename = "out_Quantity.quality")]
    pub quality: String
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Period {
    pub resolution: String,
    pub time_interval: TimeInterval,
    #[serde(rename = "Point")]
    pub point: Vec<Point>
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TimeSeries {
    pub business_type: String,
    pub curve_type: String,
    #[serde(rename = "measurement_Unit.name")]
    pub unit_name: String,
    #[serde(rename = "Period")]
    pub period: Vec<Period>
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MyEnergyDataMarketDocument {
    pub created_date_time: DateTime<Utc>,
    #[serde(rename = "period.timeInterval")]
    pub period_time_interval: TimeInterval,
    #[serde(rename = "TimeSeries")]
    pub time_series: Vec<TimeSeries>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MyEnergyResult {
    #[serde(rename = "MyEnergyData_MarketDocument")]
    pub data: MyEnergyDataMarketDocument,
    pub error_code: String,
    pub error_text: String,
    pub success: bool
}

// Most results from elnet are wrapped into the structure
#[derive(Debug, Deserialize)]
pub struct ApiResult<T> {
    pub result: Vec<T>
}

// the primay elnet api struct 
pub struct Api {
    access_token: String,
}

impl Api {
    pub fn new() -> Self {
        Api {
            access_token: "".to_string(),
        }
    }

    fn api_url(&self, subpath: &str) -> Result<Url> {
        let mut url = Url::parse(BASE_API_URL)?;

        let p = Path::new((&url).path());

        if let Some(new_path) = p.join(subpath).to_str() {
            url.set_path(new_path);
            Ok(url)
        } else {
            Err(Error::msg("subpath not valid"))
        }
    }

    // make a request using the access token given
    fn request_get<R>(&self, url: Url) -> Result<R>
    where
        R: DeserializeOwned
    {
        let client = reqwest::blocking::Client::new();

        println!("request get url {}", url);

        let res = client
            .get(url)
            .bearer_auth(&self.access_token)
            .header(ACCEPT, "application/json")
            .send()?;

        if res.status().is_success() {
            let json_body: R = res.json()?;

            // println!("{:#?}", json_body);
            return Ok(json_body);
        }

        Err(Error::msg(format!(
            "HTTP status {}",
            res.status().to_string()
        )))
    }

    fn request_post<R>(&self, url: Url, body: String) -> Result<R>
    where
        R: DeserializeOwned
    {
        let client = reqwest::blocking::Client::new();

        println!("request post url {}", url);
        println!("request body {}", body);

        let res = client
            .post(url)
            .body(body)
            .header(CONTENT_TYPE, "application/json" )
            .bearer_auth(&self.access_token)
            .header(ACCEPT, "application/json")
            .send()?;

        if res.status().is_success() {
            let json_body: R = res.json()?;

            // println!("{:#?}", json_body);
            return Ok(json_body);
        }

        Err(Error::msg(format!(
            "HTTP status {}",
            res.status().to_string()
        )))
    }


    // Get the access token for
    pub fn token_get(&mut self) -> Result<bool> {
        let client = reqwest::blocking::Client::new();

        // client.connect_verbose( true );

        let refresh_token = fs::read_to_string(TOKEN_FNAME)?;

        let url = self.api_url("api/token")?;

        println!("token url {}", url);

        let res = client
            .get(url)
            .bearer_auth(refresh_token.trim())
            .header(ACCEPT, "application/json")
            .send()?;

        if res.status().is_success() {
            let json_body = res.json::<HashMap<String, String>>()?;

            if let Some(token) = json_body.get("result") {
                self.access_token = token.to_string();

                return Ok(true);
            }
        } else {
            println!("http error status {}", res.status().as_str())
        }

        Ok(false)
    }

    pub fn meteringpoints(&self) -> Result<Vec<Meter>> {
        let url = self.api_url("api/meteringpoints/meteringpoints")?;

        let map = self.request_get::<ApiResult::<Meter>>(url)?;

        Ok(map.result)
    }

    pub fn meterdata(&self, meterpoints: &Vec<String>, from: &Date<Utc>, to: &Date<Utc> ) -> Result<Vec<MyEnergyResult>> {
        let path = format!("api/meterdata/gettimeseries/{}/{}/Hour", from.format("%F"), to.format( "%F") );

        let url = self.api_url(&path)?;

        let args = MeteringpointsArgs{
            meteringpoints: Meteringpoints {
                metering_point: meterpoints.to_vec()
            }
        };

        let body = serde_json::to_string( &args )?;

        let map = self.request_post::<ApiResult::<MyEnergyResult>>(url, body)?;

        return Ok( map.result )
    }

    pub fn access_token_get(&self) -> &str {
        self.access_token.as_str()
    }
}
