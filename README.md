# Power control

Simple tool to monitor electric power as provided by danish elnet

The goal is better consumption overview and optimal solar power usage. The more solar energy stored in the Tesla battery the cheaper mileage.

* Collect online consumption status (metrics export for Prometheus)
* Get some data from north pool
* Get power output from solar 
* Control Tesla car charging on solar surplus 
* Setup on NAS docker 


## elnet token

https://api.eloverblik.dk/CustomerApi/swagger

Get a token from `eloverblik.dk` and store it in the file `config/token.txt` 